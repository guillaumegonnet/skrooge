#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_PRINT ::..")

PROJECT(plugin_print)

IF(SKG_WEBENGINE)
    MESSAGE( STATUS "     Mode WebEngine")
    ADD_DEFINITIONS(-DSKG_WEBENGINE=${SKG_WEBENGINE})
ENDIF(SKG_WEBENGINE)
IF(SKG_WEBKIT)
    MESSAGE( STATUS "     Mode Webkit")
    ADD_DEFINITIONS(-DSKG_WEBKIT=${SKG_WEBKIT})
ENDIF(SKG_WEBKIT)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skg_print_SRCS
	skgprintplugin.cpp)


ki18n_wrap_ui(skg_print_SRCS skgprintpluginwidget_pref.ui)
kconfig_add_kcfg_files(skg_print_SRCS skgprint_settings.kcfgc )

KCOREADDONS_ADD_PLUGIN(skg_print SOURCES ${skg_print_SRCS} INSTALL_NAMESPACE "skg_gui" JSON "metadata.json")
TARGET_LINK_LIBRARIES(skg_print KF5::Parts Qt5::PrintSupport skgbasemodeler skgbasegui)

########### install files ###############
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skg_print.rc  DESTINATION  ${KDE_INSTALL_KXMLGUI5DIR}/skg_print )
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skgprint_settings.kcfg  DESTINATION  ${KDE_INSTALL_KCFGDIR} )
