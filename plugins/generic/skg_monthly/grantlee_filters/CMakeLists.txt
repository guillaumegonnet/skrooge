#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE GRANTLEE_FILTERS ::..")

PROJECT(grantlee_filters)

FIND_PACKAGE(Grantlee5 0.5 REQUIRED)

ADD_LIBRARY(grantlee_skgfilters MODULE 
skggrantleefilters.cpp 
skgdocumentfilter.cpp 
skgobjectfilter.cpp)
SET_PROPERTY(TARGET grantlee_skgfilters PROPERTY EXPORT_NAME skggrantleefilters)
TARGET_LINK_LIBRARIES(grantlee_skgfilters KF5::CoreAddons Qt5::Core skgbasemodeler skgbasegui)
GRANTLEE_ADJUST_PLUGIN_NAME(grantlee_skgfilters)

########### install files ###############
INSTALL(TARGETS grantlee_skgfilters LIBRARY DESTINATION ${KDE_INSTALL_QTPLUGINDIR}/grantlee/${Grantlee5_VERSION_MAJOR}.${Grantlee5_VERSION_MINOR} )
