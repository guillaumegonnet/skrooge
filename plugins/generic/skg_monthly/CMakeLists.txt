#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_MONTHLY ::..")

PROJECT(plugin_monthly)

IF(SKG_WEBENGINE)
    MESSAGE( STATUS "     Mode WebEngine")
    ADD_DEFINITIONS(-DSKG_WEBENGINE=${SKG_WEBENGINE})
ENDIF(SKG_WEBENGINE)
IF(SKG_WEBKIT)
    MESSAGE( STATUS "     Mode Webkit")
    ADD_DEFINITIONS(-DSKG_WEBKIT=${SKG_WEBKIT})
ENDIF(SKG_WEBKIT)

ADD_SUBDIRECTORY(grantlee_filters)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skg_monthly_SRCS
	skgmonthlyplugin.cpp
	skgmonthlypluginwidget.cpp)

ki18n_wrap_ui(skg_monthly_SRCS skgmonthlypluginwidget_base.ui)

KCOREADDONS_ADD_PLUGIN(skg_monthly SOURCES ${skg_monthly_SRCS} INSTALL_NAMESPACE "skg_gui" JSON "metadata.json")
TARGET_LINK_LIBRARIES(skg_monthly KF5::Parts KF5::ItemViews KF5::NewStuff KF5::Archive Qt5::PrintSupport skgbasemodeler skgbasegui)

########### install files ###############
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skg_monthly.rc  DESTINATION  ${KDE_INSTALL_KXMLGUI5DIR}/skg_monthly )
