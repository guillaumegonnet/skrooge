# A personal finances manager, powered by KDE

<img src="https://skrooge.org/sites/skrooge.org/files/images/header1.png" title="Juk logo">

Skrooge allows you to manage your personal finances, powered by [KDE](http://www.kde.org). Thanks to its many [features](https://skrooge.org/features), it is one of the most powerful way to enter, follow, and analyze your expenses.

Based on its KDE foundations, Skrooge can run on many platforms, including of course linux, BSD, Solaris, but also on Mac OS, and possibly on Windows.

Learn more about [skrooge.org](https://skrooge.org/).

## Features

### Import your accounts from many sources

Skrooge is able to import transactions from many formats (AFB120, QIF, CSV, MT940, OFX, QFX).  
For a more reach import, Skrooge is able to import documents from many applications ([KMYMONEY](https://kmymoney.org/), Microsoft Money, [GNUCASH](http://gnucash.org/), [GRISBI](http://fr.grisbi.org/), [HOMEBANK](http://homebank.free.fr/fr/index.php) and [MONEY MANAGER EX](https://www.moneymanagerex.org/)).

And better, Skrooge is able to import directly transactions from all your banks web sites in one click.

### Reporting

Build the graph you want to well undestand how your spend your money.  
Have a look to the periodic reports (monthly, annually, ...) to undestand the progress.  
Have a quick look on the dashboard.  
Skrooge is also able to give you advice based on your behavior.  

### Like in a web browser

Several tabs to help you organize your work.  
Bookmark your preferred reports, graphs, filters, pages, ...

### Budget

Budgeting isn’t about restriction. It’s about setting and reaching your goals. Skrooge can help you to manage your budgets by putting in places simples rules.

### Classical features

Infinite categories levels.  
Scheduled operations.  
Multi currencies.  
Manage payees.

### Advanced features

Infinite undo/redo (even after the file was closed !)  
Mass update of operations.  
Automatically process operations based on search conditions.  
Instant filtering on operations and reports.  
Download of quotes.  
Add all properties you want on all objets (transactions, accounts, categories, ...) and including files (pdf, pictures, ...).

### Track refund of your expenses

Skrooge can help you to check that you have received the expected refund (e.g. medical).

## How to install it:

More information here: [https://skrooge.org/download](https://skrooge.org/download)

## How to build it:

### Install prerequisits:

 - xsltproc
 - libgrantlee5-dev (>=5.0.0)
 - libsqlite3-dev (>=3.7.0)
 - libofx-dev 
 - libboost-dev 
 - qtscript5-dev
 - qttools5-dev
 - libqt5webkit5-dev  (If you want to build with WebKit. You must not activate the option SKG_WEBENGINE) (**Preferred and default**)
 - libqt5webengine5-dev (If you want to build with WebEngine. You must activate the option SKG_WEBKIT)
 - libqt5svg5-dev
 - libkf5coreaddons-dev
 - libkf5archive-dev
 - libkf5xmlgui-dev
 - libkf5wallet-dev
 - libkf5parts-dev
 - libkf5newstuff-dev
 - libkf5iconthemes-dev 
 - libkf5kdelibs4support-dev
 - libkf5notifyconfig-dev  	
 - libkf5runner-dev  
 - plasma-framework-dev  
 - libkf5doctools-dev  	
 - kgendesignerplugin 
 - libqca-qt5-2-dev 
 - libkf5activities-dev
 - kross-dev 
 - qtdeclarative5-dev
 - qtquickcontrols2-5-dev
 - libsqlcipher-dev
 - sqlcipher
 - kirigami2-dev

	
#### Examples:

On ubuntu:

    sudo apt-get install pkg-config build-essential cmake devscripts cdbs extra-cmake-modules kross-dev sqlite3 sqlcipher libsqlcipher-dev libgrantlee5-dev libsqlite3-dev libofx-dev libboost-dev xsltproc qtscript5-dev qttools5-dev libqt5webkit5-dev libqt5svg5-dev libkf5coreaddons-dev libkf5archive-dev libkf5xmlgui-dev libkf5activities-dev libkf5wallet-dev libkf5parts-dev libkf5newstuff-dev libkf5iconthemes-dev libkf5kdelibs4support-dev libkf5notifyconfig-dev libkf5runner-dev libkf5doctools-dev kgendesignerplugin libqca-qt5-2-dev qtbase5-private-dev qtquickcontrols2-5-dev kirigami2-dev libkf5plasma-dev qtwebengine5-dev qtdeclarative5-dev

### Extract the archive, and enter the "skrooge" directory. Then run :

	mkdir build && cd build
        cmake .. -DCMAKE_INSTALL_PREFIX=[path to your KDE installation]
	make

#### Examples:

On ubuntu:

		mkdir build && cd build
                cmake .. -DCMAKE_INSTALL_PREFIX=`kf5-config --prefix` -DQT_PLUGIN_INSTALL_DIR=`kf5-config --qt-plugins` -DCMAKE_BUILD_TYPE=release -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DSKG_WEBENGINE=OFF -DSKG_BUILD_TEST=OFF -DSKG_DESIGNER=OFF 
		make
		

On ubuntu (with ninja):

		mkdir build && cd build
                                                                                                                                                                                                                                                                release -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DSKG_WEBENGINE=OFF -DSKG_WEBKIT=ON -DSKG_BUILD_TEST=OFF -DSKG_DESIGNER=OFF 
		make
        
        

### Prepare to run

        Skrooge uses plugins. By default, plugins are loaded from your system. It means that, if you have a local build of Skrooge and an other version of Skrooge installed on your system. The system version of plugins will be loaded.
        The easiest solution is to install your local build like this:
                make install (as root)
                
#### Examples:

On ubuntu:

    sudo make install 

If you don't want to install Skrooge on your system, you will have to set many Qt and Kde environment variables before running skrooge. This is not easy to do.

### Run the test

    ctest

#### Run skrooge

    /usr/bin/skrooge
    
## Other important thinks to remember

### For Static code analysis (scan-build)

    CXX=clang++ CC=clang scan-build -k cmake .. -DCMAKE_INSTALL_PREFIX=`kf5-config --prefix` -DCMAKE_BUILD_TYPE=profile
    make clean
    scan-build -k -o results make

### With sanitizer (-fno-omit-frame-pointer )

    cmake .. -DCMAKE_INSTALL_PREFIX=`kf5-config --prefix` -DQT_PLUGIN_INSTALL_DIR=`kf5-config --qt-plugins` -DCMAKE_BUILD_TYPE=profile -DECM_ENABLE_SANITIZERS='undefined,address,asan,ubsan'
    export ASAN_OPTIONS=detect_leaks=1

### With clazy

    export CXX="clazy"
    export CLAZY_CHECKS="level0,level1,level2,level3"
    export CLAZY_FIXIT="fix-qlatin1string-allocations,fix-fromLatin1_fromUtf8-allocations,fix-fromCharPtrAllocations"
    cmake .. -DCMAKE_CXX_COMPILER=clazy -DCMAKE_INSTALL_PREFIX=`kf5-config --prefix` -DQT_PLUGIN_INSTALL_DIR=`kf5-config --qt-plugins` -DCMAKE_BUILD_TYPE=Debug
    
    make clean
    make -i > t.txt 2>&1
    cat t.txt | grep "warning:" | grep -v "ui_" | grep -v "moc_" | grep -v "_settings" |grep -v "/usr/include" | grep -v "FixIt failed" | grep -v ".moc:" | grep -v "/build/" | sort -u > warning_all.txt 
    cat warning_all.txt | grep -v "ctor-missing-parent-argument" | grep -v "clazy-copyable-polymorphic" | sort -u > warning.txt 

### With clazy-standalone

    cmake .. -DCMAKE_INSTALL_PREFIX=`kf5-config --prefix` -DQT_PLUGIN_INSTALL_DIR=`kf5-config --qt-plugins` -DCMAKE_BUILD_TYPE=release -DCMAKE_EXPORT_COMPILE_COMMANDS=1 
    make
    find . -name "*cpp" | xargs clazy-standalone -checks=level2 -p compile_commands.json > t.txt 2>&1
    cat t.txt | grep "warning:" | grep -v "ui_" | grep -v "moc_" | grep -v "_settings" |grep -v "/usr/include" | grep -v "FixIt failed" | sort -u > warning.txt

### clang-tidy

    run-clang-tidy.py -header-filter='.*' -checks=*,-fuchsia-*,-objc-*,-android-*,-abseil-*,-google-*,-misc-unused-parameters,-clang-diagnostic-error,-cppcoreguidelines-pro-type-member-init,-llvm-header-guard,-readability-inconsistent-declaration-parameter-name,-readability-redundant-member-init -fix > traces.txt
     
    cat traces.txt | grep warning  | grep -v ui_ | grep  -v /build/ | grep -v "clang-tidy -header" | sort -u > clangtidy.txt

### With coverity

    export PATH=~/Telechargements/cov-analysis-linux64-2020.09/bin/:$PATH
    make clean
    cov-build --dir cov-int make -j 4
    tar caf skrooge.bz2 cov-int

### iwyu

    iwyu_tool.py -p .

### Docker

    sudo docker build -t skrooge .
    sudo docker run -e DISPLAY=unix$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix skrooge

### Flatpak

[https://community.kde.org/Guidelines_and_HOWTOs/Flatpak]

#### Build

    mkdir app repo
    flatpak install flathub org.kde.Platform//5.12
    flatpak install flathub org.kde.Sdk//5.12
    flatpak install flathub io.qt.qtwebkit.BaseApp//5.12
    flatpak-builder --ccache --repo=repo --subject="Build of skrooge `date`" skrooge org.kde.skrooge.json --force-clean
    
#### Install

    flatpak uninstall org.kde.skrooge
    flatpak install skrooge org.kde.skrooge
    
#### Run    

    flatpak run org.kde.skrooge
    
#### Debug    

    flatpak run --command=sh --devel org.kde.skrooge

### Convert all logo in 100x100

    for x in /home/s/Developpements/skrooge/images/logos/*.png
    do
        convert $x -resize 100x100 $x
    done
